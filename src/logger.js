import { createLogger, transports, format } from 'winston';

// Configure logger
const logger = createLogger({
    level: process.env.LOG_LEVEL,
    defaultMeta: { service: process.env.SERVICE_NAME },
    format: format.combine(
        format.timestamp(),
        format.splat(),
        format.json(),
    ),
    transports: [
        new transports.File({
            filename: process.env.LOGFILE,
            level: 'info',
        }),
        new transports.File({
            filename: process.env.ERRORLOG,
            level: 'error',
        }),
    ],
});

// Add console output if we're in dev environment
if(process.env.NODE_ENV.trim() == 'dev') {
    logger.add(
        new transports.Console({
            format: format.combine(
                format.timestamp(),
                format.colorize(),
                format.splat(),
                format.prettyPrint(),
                format.simple()
            ),
            level: 'debug',
        })
    );
    logger.add(
        new transports.File({
            filename: process.env.DEBUGLOG,
            level: 'debug',
        })
    );
}

export default logger;
