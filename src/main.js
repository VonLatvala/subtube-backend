import 'dotenv/config';

import logger from './logger';

const main = (argc, argv) => { // eslint-disable-line
    logger.info('Software started.');
};

main(process.argc, process.argv);
