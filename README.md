# SubTube backend

## What is this

Backend component of SubTube, an integration between subsonic and youtube-dl, to easily add music to subsonic from youtube.

## Installation

Firstly, install the dependencies

```sh
yarn install
```

Then you can build and package the software by issuing

```sh
yarn package
```

The packaged software can be found in `./builds`, and the raw built sofware in `./dist`.
